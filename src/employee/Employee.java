package employee;

import salary.Salary;

public class Employee {
	
	private int id;
	private String name;
	private Salary salary;

	public Employee(int id, String name, Salary salary) {
		setId(id);
		setName(name);
		setSalary(salary);
	}

	public  String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Salary getSalary() {
		return salary;
	}

	public void setSalary(Salary salary) {
		this.salary = salary;
	}
}
