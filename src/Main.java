

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import employee.Employee;
import salary.HourlySalary;
import salary.MontlySalary;

public class Main {

	public static void main(String[] args) {		

		List<Employee> employees = new ArrayList<>();
		employees.add(new Employee(1, "Wix", new HourlySalary(210.0)));
		employees.add(new Employee(2, "John", new MontlySalary(33280.0)));
		employees.add(new Employee(3, "Bob", new HourlySalary(200.0)));
		employees.add(new Employee(4, "Mario", new MontlySalary(33820.0)));
		employees.add(new Employee(5, "Sam", new HourlySalary(200.0)));
		employees.add(new Employee(6, "Alba", new MontlySalary(32380.0)));
		employees.add(new Employee(7, "Sergio", new HourlySalary(100.0)));
		employees.add(new Employee(8, "Nat", new MontlySalary(33280.0)));	
		
		System.out.println("====== begin array");	
		for (Employee employee : employees) {
			System.out.println(employee.getId() + ". "+employee.getName());
		}
		
		
		Collections.sort(employees, new Comparator<Employee>() {
			@Override
			public int compare(Employee e1, Employee e2) {
				return e1.getName().compareTo(e2.getName());
			}
		});
		
		System.out.println("====== sort array by name");
		for (Employee employee : employees) {
			System.out.println(employee.getId() + ". "+employee.getName()+ " :" +employee.getSalary().calculate());
		}
		

		System.out.println("====== sort array by salary and name");		
		Collections.sort(employees, new Comparator<Employee>() {
			@Override
			public int compare(Employee o1, Employee o2) {				
				return Double.compare(o2.getSalary().calculate(), o1.getSalary().calculate());
			}			
		});	
		
		for (Employee employee : employees) {
			System.out.println(employee.getId() + ". "+employee.getName()+ " :" +employee.getSalary().calculate());
		}
	}
}
