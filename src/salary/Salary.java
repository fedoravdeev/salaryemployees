package salary;

public interface Salary {
	public double calculate();
}