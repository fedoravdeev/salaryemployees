package salary;

public class MontlySalary implements Salary {

	private double monthlyWage;
	
	public MontlySalary(double monthlyWage) {
		setMonthlyWage(monthlyWage);
	}

	@Override
	public double calculate() {
		return monthlyWage;
	}

	public double getMonthlyWage() {
		return monthlyWage;
	}

	public void setMonthlyWage(double monthlyWage) {
		this.monthlyWage = monthlyWage;
	}	
}