package salary;

public class HourlySalary implements Salary {

	public double rate;	
	private final int HOURS_PER_DAY = 8;
	private final double AVERAGE_DAY = 20.8;	
	
	public HourlySalary(double rate) {
		setRate(rate);
	}

	@Override
	public double calculate() {		  
		return getRate() * HOURS_PER_DAY * AVERAGE_DAY;
	}

	public double getRate() {
		return rate;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}
}